<?php

/**
 * @file
 * This file contains all the admin-related callbacks.
 *
 * Overview page - provides a list of existing add to head profiles.
 */

/**
 * This function provides the edit form.
 */
function mockingfish_integration_edit_profile($form, &$form_state) {
  $settings = variable_get('mockingfish_integration_code', '');
  $form['code'] = array(
    '#type' => 'textarea',
    '#title' => t('Code'),
    '#description' => t('Enter the MockingFish Code you would like to insert into the head of the page'),
    '#required' => TRUE,
    '#default_value' => $settings,
    '#wysiwyg' => FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate handler for the add/edit form.
 */
function mockingfish_integration_edit_profile_validate($form, &$form_state) {
  $settings = variable_get('mockingfish_integration_code', '');
}

/**
 * Submit handler for the add/edit form.
 */
function mockingfish_integration_edit_profile_submit($form, &$form_state) {
  $settings = variable_get('mockingfish_integration_code', '');
  $settings = trim($form_state['values']['code']);
  variable_set('mockingfish_integration_code', $settings);

  $form_state['redirect'] = 'admin/config/development/mockingfish-integration';
}
